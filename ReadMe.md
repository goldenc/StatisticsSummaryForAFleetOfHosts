

## Required Tools Setup
1. Install Java 1.8 (https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html)

2. Install Maven (https://maven.apache.org/install.html)

## Assumptions Made

1. Only M1, M2, or M3 Instance Types Are Available

2. The Task Solution Will Be Run By Someone With Software Development Skills.

3. Host SlotStates(<nState>) Can Not Be More Than the Total Number Of Slots (<N>) On the Machine

