package com.golden.statistics.summary.filemanager

import com.golden.statistics.summary.domain.Host
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title
import spock.lang.Unroll

import java.nio.file.Path
import java.nio.file.Paths

@Title('Unit Tests For FleetStateFileReader')
@Subject(FleetStateFileReader.class)
class FleetStateFileReaderSpec extends  Specification{

    FleetStateFileReader fleetStateFileReader = new FleetStateFileReader()

    def'Should Create A Host From A Given String'(){

        given:'A String Containing The State Of A Host'
            String hostState = '10,M1,4,0,1,0,1'

        when:'FleetStateFileReader.createHostFromFileLine Is Called'
        Host responseHost = fleetStateFileReader.createHostFromFileLine(hostState)

        then:'A Host Should Be Created With The Parameters Below'
        responseHost.id == 10
        responseHost.instanceType == 'M1'
        responseHost.numberOfSlots == 4
        responseHost.slotStates[0] == 0
        responseHost.slotStates[1] == 1
        responseHost.slotStates[2] == 0
        responseHost.slotStates[3] == 1
    }

    def'Should Create A List Of Host Objects From A Text File That Contains The State Of A Fleet'(){
        given:'A File That Contains The State Of The Fleet'
        Path path = Paths.get(Paths.get("").toAbsolutePath().toString()
                +"/src/test/resources/FleetStateTestData.txt")

        when:'fleetStateFileReader.readFromFileToListCollection is called'
        List<Host> hosts = fleetStateFileReader.readFromFileToListCollection(path)

        then:'A List OF Host Should Be Created'
        hosts.size() == 2
        hosts[0].id == 10
    }

    @Unroll
    def 'Should Throw A FleetStateFileReaderException If File Cannot Be Read'(){

        when:'fleetStateFileReader.readFromFileToListCollection Is Called With An Invalid Path'
        fleetStateFileReader.readFromFileToListCollection(invalidPath)

        then:'A FleetStateFileReaderException Should Be Thrown'
        thrown(FleetStateFileReaderException.class)

        where:'The Various Invalid Paths Are'
        invalidPath << [ Paths.get(Paths.get("").toAbsolutePath().toString()
                +"/src/test/resources/FileNotFound.txt"),
                         Paths.get("")]
    }

    @Unroll
    def'Should Throw A FleetStateFileReaderException If Malformed Input Is Passed'(){
        when:'FleetStateFileReader.createHostFromFileLine Is Called'
        fleetStateFileReader.createHostFromFileLine(malformedInput)

        then:'A FleetStateFileReaderException Should Be Thrown'
        thrown(FleetStateFileReaderException.class)

        where:'The Various Malformed Inputs Are'
        malformedInput << ['',
                          '10,M1,4,0,1,0,1,1',
                           '10,M1,1,0,1,0,1,1']
    }

    @Unroll
    def'Should Throw A FleetStateFileReaderException If File Contains Malformed Input'(){
        when:'fleetStateFileReader.readFromFileToListCollection is called'
        fleetStateFileReader.readFromFileToListCollection(path)

        then:'A FleetStateFileReaderException Should Be Thrown'
        thrown(FleetStateFileReaderException.class)

        where:'The Various Malformed Input Test Files Are'
        path << [Paths.get(Paths.get("").toAbsolutePath().toString()
                +"/src/test/resources/MalformedTestFile.txt"),
                             Paths.get(Paths.get("").toAbsolutePath().toString()
                +"/src/test/resources/MalformedTestFile2.txt")]
    }

}
