package com.golden.statistics.summary.filemanager

import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

import java.nio.file.Path
import java.nio.file.Paths

@Title("Unit Tests For StatisticsSummaryFileWriter")
@Subject(StatisticsSummaryFileWriter)
class StatisticsSummaryFileWriterSpec extends  Specification {
    StatisticsSummaryFileWriter statisticsSummaryFileWriter = new StatisticsSummaryFileWriter()
    def'Should Write A Given String Builder To A File'(){
        given:'A String Builder'
        StringBuilder statisticsSummary = new StringBuilder(String.format("EMPTY: M1=0; M2=0; M3=2;\n" +
                "FULL: M1=0; M2=2; M3=0;\n" +
                "MOST FILLED: M1=1,1; M2=0,0; M3=0,0;"))

        when:'statisticsSummaryFileWriter'
        statisticsSummaryFileWriter.writeStatisticsToFile(statisticsSummary)

        then:''
        new File("Statistics.txt").exists()

    }
}
