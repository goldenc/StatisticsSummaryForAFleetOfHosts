package com.golden.statistics.summary.statisticssummary

import com.golden.statistics.summary.domain.Host
import com.golden.statistics.summary.domain.VirtualInstanceType
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title
import spock.lang.Unroll

@Title('Unit Tests For FleetStatisticsSummary')
@Subject(FleetStatisticsSummary)
class FleetStatisticsSummarySpec extends  Specification {

    FleetStatisticsSummary fleetStatisticsSummary = new FleetStatisticsSummary(
            new VirtualInstanceType(), new VirtualInstanceType(), new VirtualInstanceType())

    @Unroll
    def'Should Add All Slot States For A Host and Return The Total'(){
        given:''
        Host host = new Host(id, instanceType, numberOfSlots,slotState);

        when:'fleetStatisticsSummary.addSlotStatesForAHost is called'
        long total = fleetStatisticsSummary.addSlotStatesForAHost(host)

        then:'The Total Of All Slots Must Be Returned'
        total == expectedOutput

        where:'The Given Host Properties Are'

        id | instanceType | numberOfSlots | slotState | expectedOutput
        1 | "M1" | 4 | Arrays.asList(1,1,0,0).toList() | 2
        2 | "M1" | 3 | Arrays.asList(1,1,1).toList() | 3
        3 | "M3" | 5 | Arrays.asList(0,0,0,0,0).toList() | 0
    }

    @Unroll
    def'Should Calculate Statistics For A Given List Of Hosts'(){
        given:'A List Of Hosts'
        List<Host> totalHosts = new ArrayList<>(someHosts)

        when:'fleetStatisticsSummary.calculateSummaryStatistics Is Called With A List Of Hosts'
        StringBuilder actualResponse = fleetStatisticsSummary.calculateSummaryStatistics(totalHosts)

        then:''
        actualResponse.contains(expectedResponse)

        where:''
        someHosts | expectedResponse
        sampleHosts() | getSampleOutputStatistics()
        sampleHostsList() | getMoreSampleOutputStatistics()
    }

    private String getSampleOutputStatistics() {
        return String.format("EMPTY: M1=0; M2=0; M3=1;\n" +
                "FULL: M1=0; M2=1; M3=0;\n" +
                "MOST FILLED: M1=1,2; M2=0,0; M3=0,0;")
    }

    private String getMoreSampleOutputStatistics() {
        return String.format("EMPTY: M1=0; M2=0; M3=2;\n" +
                "FULL: M1=0; M2=2; M3=0;\n" +
                "MOST FILLED: M1=1,1; M2=0,0; M3=0,0;")
    }

    public List<Host> sampleHosts(){
        List<Host> hosts = new ArrayList<>()
        hosts.add(new Host(1,"M1",4,Arrays.asList(1,1,0,0).toList()))
        hosts.add(new Host(2,"M2",4,Arrays.asList(1,1,1,1).toList()))
        hosts.add(new Host(3,"M3",4,Arrays.asList(0,0,0,0).toList()));
        return  hosts;
    }

    public List<Host> sampleHostsList(){
        List<Host> hosts = new ArrayList<>()
        hosts.add(new Host(1,"M1",4,Arrays.asList(1,1,0,0).toList()))
        hosts.add(new Host(2,"M2",4,Arrays.asList(1,1,1,1).toList()))
        hosts.add(new Host(3,"M3",4,Arrays.asList(0,0,0,0).toList()));
        hosts.add(new Host(4,"M1",4,Arrays.asList(1,1,0,0).toList()))
        hosts.add(new Host(5,"M2",5,Arrays.asList(1,1,1,1,1).toList()))
        hosts.add(new Host(6,"M3",4,Arrays.asList(0,0,0,0).toList()));
        hosts.add(new Host(7,"M1",4,Arrays.asList(1,1,1,0).toList()));
        return  hosts;
    }

}
