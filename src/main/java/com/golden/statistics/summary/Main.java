package com.golden.statistics.summary;

import com.golden.statistics.summary.domain.Host;
import com.golden.statistics.summary.domain.VirtualInstanceType;
import com.golden.statistics.summary.filemanager.FleetStateFileReader;
import com.golden.statistics.summary.filemanager.FleetStateFileReaderException;
import com.golden.statistics.summary.filemanager.StatisticsSummaryFileWriter;
import com.golden.statistics.summary.statisticssummary.FleetStatisticsSummary;

import java.nio.file.Paths;
import java.util.List;

public class Main {

    public static  void main(String[] args){

        if(args.length>0) {
            System.out.println("*************Calculating Summary Statistics For A Fleet Of Hosts***********");

            FleetStateFileReader fleetStateFileReader = new FleetStateFileReader();
            FleetStatisticsSummary fleetStatisticsSummary = new FleetStatisticsSummary(new VirtualInstanceType(),
                    new VirtualInstanceType(), new VirtualInstanceType());
            StatisticsSummaryFileWriter statisticsSummaryFileWriter = new StatisticsSummaryFileWriter();

            List<Host> hosts = fleetStateFileReader.readFromFileToListCollection(Paths.get(args[0]));
            StringBuilder statisticsSummary = fleetStatisticsSummary.calculateSummaryStatistics(hosts);
            statisticsSummaryFileWriter.writeStatisticsToFile(statisticsSummary);

            System.out.println(statisticsSummary);
            System.out.println("*******************Summary Statistics Have Been Written To FIle Statistics.txt**********************");
        }
        throw new FleetStateFileReaderException("System Started Without Input File Path");

    }
}
