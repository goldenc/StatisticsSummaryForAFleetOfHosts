package com.golden.statistics.summary.filemanager;

import com.golden.statistics.summary.domain.Host;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FleetStateFileReader {

    private final String HOST_STATE_COMMA_SEPERATOR = ",";
    private final int MINIMUM_LENGTH = 4;

    public List<Host> readFromFileToListCollection(Path path){
        List<Host> totalHosts = new ArrayList<>();

        try (Stream<String> stream = Files.lines(path)) {
            stream
                    .map(String::trim)
                    .forEach(host -> {
                        totalHosts.add(createHostFromFileLine(host));
                    });
            return totalHosts;
        }catch (IOException ex){
            throw new FleetStateFileReaderException("Unable To Read File", ex);
        }catch (Exception ex){
            throw new FleetStateFileReaderException("Error while Reading File", ex);
        }
    }

    public Host createHostFromFileLine(String currentLine){
        String [] hostFromFile = currentLine.split(HOST_STATE_COMMA_SEPERATOR);
        int hostFromFileLength = hostFromFile.length;
        if(hostFromFileLength <= MINIMUM_LENGTH || Integer.valueOf(hostFromFile[2])!=hostFromFileLength-3){
            throw new FleetStateFileReaderException(String.format("Error at line %s :", currentLine));
        }
        int id = Integer.valueOf(hostFromFile[0]);
        String instanceType = hostFromFile[1];
        int numberOfSlots = Integer.valueOf(hostFromFile[2]);
        List<Integer> slotState = Arrays.asList(hostFromFile)
                .subList(3, hostFromFile.length)
                    .stream().map(Integer::valueOf).collect(Collectors.toList());
        return new Host(id, instanceType, numberOfSlots,slotState);
    }
}