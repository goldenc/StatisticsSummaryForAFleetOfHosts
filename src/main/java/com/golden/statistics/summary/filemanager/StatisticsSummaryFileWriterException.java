package com.golden.statistics.summary.filemanager;


public class StatisticsSummaryFileWriterException extends RuntimeException{

    public StatisticsSummaryFileWriterException(String message, Exception exception) {
        super(message, exception);
    }
}
