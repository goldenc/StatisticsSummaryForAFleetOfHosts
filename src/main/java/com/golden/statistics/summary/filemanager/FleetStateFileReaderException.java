package com.golden.statistics.summary.filemanager;

public class FleetStateFileReaderException extends RuntimeException{
    public  FleetStateFileReaderException(String message){
        super(message);
    }

    public FleetStateFileReaderException(String message, Exception exception) {
        super(message, exception);
    }
}
