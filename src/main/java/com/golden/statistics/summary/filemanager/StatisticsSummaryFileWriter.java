package com.golden.statistics.summary.filemanager;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;


public class StatisticsSummaryFileWriter {

    public void writeStatisticsToFile(StringBuilder statisticsSummary){

        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream("Statistics.txt"))) {
            writer.append(statisticsSummary);
            writer.flush();
            writer.close();
        }catch (Exception ex){
            new StatisticsSummaryFileWriterException("Error While Writing To File. ", ex);
        }
    }
}
