package com.golden.statistics.summary.statisticssummary;

import com.golden.statistics.summary.domain.Host;
import com.golden.statistics.summary.domain.VirtualInstanceType;

import java.util.List;

public class FleetStatisticsSummary {

    private VirtualInstanceType m1Instance;
    private VirtualInstanceType m2Instance;
    private VirtualInstanceType m3Instance;
    private final String M1_INSTANCE_TYPE = "M1";
    private final String M2_INSTANCE_TYPE = "M2";
    private final String M3_INSTANCE_TYPE = "M3";

    public  FleetStatisticsSummary(VirtualInstanceType m1Instance,
                                  VirtualInstanceType m2Instance, VirtualInstanceType m3Instance){
        this.m1Instance = m1Instance;
        this.m2Instance = m2Instance;
        this.m3Instance = m3Instance;
    }

    public StringBuilder calculateSummaryStatistics(List<Host> totalHosts){

        totalHosts.forEach(host -> {

            if (host.getInstanceType().equals(M1_INSTANCE_TYPE)) {
                calculateSummaryStatisticsForInstanceType(m1Instance, host);
            }
            if (host.getInstanceType().equals(M2_INSTANCE_TYPE)) {
                calculateSummaryStatisticsForInstanceType(m2Instance, host);
            }
            if (host.getInstanceType().equals(M3_INSTANCE_TYPE)) {
                calculateSummaryStatisticsForInstanceType(m3Instance, host);
            }
        });

        return prepareStatisticsSummary();
    }

    public void calculateSummaryStatisticsForInstanceType(VirtualInstanceType instanceType, Host host){

        int sumOfSlotStates = addSlotStatesForAHost(host);
        int numberOfSlots = host.getNumberOfSlots();
        int numberOfEmptySlotsAbove0 = numberOfSlots - sumOfSlotStates;
        if(sumOfSlotStates==0)
        {
            instanceType.incrementNumberOfEmptyHosts();
        }else if(sumOfSlotStates== numberOfSlots){
                instanceType.incrementNumberOfFullHosts();
            }
        else if(numberOfEmptySlotsAbove0==instanceType.getSmallestNumberOfEmptySlotsAbove0()){
                instanceType.incrementMostFilledCounts(numberOfEmptySlotsAbove0);
        }
        else if(instanceType.getSmallestNumberOfEmptySlotsAbove0()==0){
            instanceType.resetMostFilledCounts(numberOfEmptySlotsAbove0);
        }
        else if(numberOfEmptySlotsAbove0<instanceType.getSmallestNumberOfEmptySlotsAbove0()){
            instanceType.resetMostFilledCounts(numberOfEmptySlotsAbove0);
        }
    }

    public int addSlotStatesForAHost(Host host){
        int total = 0;
        for(int state: host.getSlotStates()){
            total += state;
        }
        return  total;
    }

    public StringBuilder prepareStatisticsSummary(){
        StringBuilder statisticsSummary = new StringBuilder();

        statisticsSummary.append(getEmptyHostsStatistics());
        statisticsSummary.append(getFullHostsStatistics());
        statisticsSummary.append(getMostFilledHostsStatistics());

        return  statisticsSummary;
    }

    protected String getEmptyHostsStatistics() {
        return String.format("EMPTY: M1=%s; M2=%s; M3=%s;\n",
                m1Instance.getNumberOfEmptyHosts(),
                m2Instance.getNumberOfEmptyHosts(),
                m3Instance.getNumberOfEmptyHosts());
    }

    protected String getFullHostsStatistics() {
        return String.format("FULL: M1=%s; M2=%s; M3=%s;\n",
                m1Instance.getNumberOfFullHosts(),
                m2Instance.getNumberOfFullHosts(),
                m3Instance.getNumberOfFullHosts());
    }

    protected String getMostFilledHostsStatistics() {
        return String.format("MOST FILLED: M1=%s,%s; M2=%s,%s; M3=%s,%s;",
                m1Instance.getCountOfMostFilledHosts(), m1Instance.getNumberOfEmptySlotsForMostFilledHost(),
                m2Instance.getCountOfMostFilledHosts(), m2Instance.getNumberOfEmptySlotsForMostFilledHost(),
                m3Instance.getCountOfMostFilledHosts(), m3Instance.getNumberOfEmptySlotsForMostFilledHost());
    }
}