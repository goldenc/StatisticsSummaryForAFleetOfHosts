package com.golden.statistics.summary.domain;

import java.util.List;

public final class Host {

    private final int id;
    private final String instanceType;
    private final int numberOfSlots;
    private final List<Integer> slotStates;

    public Host(int id, String instanceType, int numberOfSlots, List<Integer> slotState){
        this.id = id;
        this.instanceType = instanceType;
        this.numberOfSlots = numberOfSlots;;
        this.slotStates = slotState;
    }

    public int getId() {
        return id;
    }

    public String getInstanceType() {
        return instanceType;
    }

    public int getNumberOfSlots() {
        return numberOfSlots;
    }

    public List<Integer> getSlotStates() {
        return slotStates;
    }
}
