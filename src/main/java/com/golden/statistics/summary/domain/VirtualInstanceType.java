package com.golden.statistics.summary.domain;


public class VirtualInstanceType {

    private int numberOfEmptyHosts;
    private int numberOfFullHosts;
    private int countOfMostFilledHosts;
    private int numberOfEmptySlotsForMostFilledHost;
    private int smallestNumberOfEmptySlotsAbove0;

    public VirtualInstanceType(){
        smallestNumberOfEmptySlotsAbove0 = 0;
    }

    public int getNumberOfEmptyHosts() {
        return numberOfEmptyHosts;
    }

    public int getNumberOfFullHosts() {
        return numberOfFullHosts;
    }

    public int getCountOfMostFilledHosts() {
        return countOfMostFilledHosts;
    }

    public int getNumberOfEmptySlotsForMostFilledHost() {
        return numberOfEmptySlotsForMostFilledHost;
    }

    public int getSmallestNumberOfEmptySlotsAbove0() {
        return smallestNumberOfEmptySlotsAbove0;
    }

    public void incrementNumberOfEmptyHosts(){
        this.numberOfEmptyHosts += 1;
    }

    public  void incrementNumberOfFullHosts(){
        this.numberOfFullHosts +=1;
    }

    public void incrementMostFilledCounts(int emptySlots){
        this.numberOfEmptySlotsForMostFilledHost += emptySlots;
        this.countOfMostFilledHosts += 1;
    }

    public void resetMostFilledCounts(int emptySlots){
        this.numberOfEmptySlotsForMostFilledHost = emptySlots;
        this.countOfMostFilledHosts = 1;
        this.smallestNumberOfEmptySlotsAbove0 = emptySlots;
    }
}
